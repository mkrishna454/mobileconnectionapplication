package com.mobile.application.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mobile.application.dto.AdminRequestDto;
import com.mobile.application.dto.AdminResponseDto;
import com.mobile.application.entity.Request;
import com.mobile.application.service.AdminService;

@RestController
public class AdminController {

	@Autowired
	AdminService adminServ;
	
	
	@GetMapping("/getallrequest")
	public List<Request> getAllRequest(){
		return adminServ.getAllRequests();
	}
	
	@PostMapping("/admin")
	public AdminResponseDto approveRequest(@Validated @RequestBody AdminRequestDto adminRequestDto) {
		Logger logger = LoggerFactory.getLogger(AdminController.class);
		
		return adminServ.approveRequest(adminRequestDto); 
	}
}
