package com.mobile.application.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mobile.application.dto.UserResponse;
import com.mobile.application.entity.Request;
import com.mobile.application.entity.User;
import com.mobile.application.service.UserService;

@RestController
public class UserController {

//Jai
	@Autowired
	UserService userServ;
	
	
	@PostMapping("/registration")
	public UserResponse userRegistration(@RequestBody @Valid User user) {
		return userServ.userRegistration(user);
	}
	
	@GetMapping("/getrequest")
	public Request getUserRequest(@RequestParam int requestId) {
		return userServ.getRequestDetails(requestId);
	}
}
