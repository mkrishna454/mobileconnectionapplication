package com.mobile.application.dto;

public class UserResponse {

	private int requestId;
	private String status;

	public UserResponse(int requestId, String status) {
		super();
		this.requestId = requestId;
		this.status = status;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
