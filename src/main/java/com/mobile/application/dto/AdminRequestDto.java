package com.mobile.application.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.springframework.stereotype.Component;

@Component
public class AdminRequestDto {

	@Min(value = 1, message = "RequestId must be GraterThan 0")
	private Integer requestId;
	@Min(value = 1, message = "UserId must be GraterThan 0")
	private int userId;
	@NotEmpty(message = "Should provide the Stratus")
	private String status;
	@NotEmpty(message = "Should provide the comments")
	private String comment;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
