package com.mobile.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MobileConnectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileConnectionApplication.class, args);
	}

}
