package com.mobile.application.service;

import java.util.List;

import com.mobile.application.dto.AdminRequestDto;
import com.mobile.application.dto.AdminResponseDto;
import com.mobile.application.entity.Request;

public interface AdminService {

	public List<Request> getAllRequests();
	
	public AdminResponseDto approveRequest(AdminRequestDto adminRequestDto);
}
