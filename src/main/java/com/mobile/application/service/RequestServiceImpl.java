package com.mobile.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobile.application.entity.Request;
import com.mobile.application.repository.RequestRepository;

@Service
public class RequestServiceImpl implements RequestService {
	
	@Autowired
	private RequestRepository requestRepository;

	@Override
	public void saveRequest(Request request) {
		requestRepository.save(request);
	}

}