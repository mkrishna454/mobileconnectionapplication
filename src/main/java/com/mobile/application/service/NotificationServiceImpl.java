package com.mobile.application.service;

import java.util.ArrayList;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.mobile.application.entity.CellNumber;
import com.mobile.application.entity.Request;
import com.mobile.application.repository.CellNumberRepository;
import com.mobile.application.repository.RequestRepository;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	RequestRepository repo;

	@Autowired
	CellNumberRepository repository;

	private List<CellNumber> approvedNumbers = new ArrayList<>();

	@Override
	@Scheduled(fixedRate = 10000)
	public void mobStatusChange() {
		List<Request> requestList = repo.findByIsNotifiedFalse();
		System.out.println("Testing schedular");
		if (null != requestList) {
			requestList.forEach(e -> {
				if (e.getStatus().equals("APPROVED")) {
					CellNumber number = repository.findByNumberId(e.getNumberId());
					if (null != number) {
						number.setStatus("Connection Enabled");
						approvedNumbers.add(number);
					}
				}
			});
		}
		if (approvedNumbers.size() > 0)
		 approvedNumbers = repository.saveAll(approvedNumbers);
		List<Request> updateList = new ArrayList<>();
		if(approvedNumbers.size()>0 && null != requestList) {
				requestList.forEach(e -> {
					e.setNotified(true);
					updateList.add(e);
				});
		}
		repo.saveAll(updateList);
	}
}
