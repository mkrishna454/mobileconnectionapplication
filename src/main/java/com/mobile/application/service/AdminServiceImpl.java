package com.mobile.application.service;

import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobile.application.dto.AdminRequestDto;
import com.mobile.application.dto.AdminResponseDto;
import com.mobile.application.entity.Request;
import com.mobile.application.entity.User;
import com.mobile.application.repository.RequestRepository;
import com.mobile.application.repository.UserRepository;
import com.mobile.application.util.UserExcetion;

@Service
public class AdminServiceImpl implements AdminService {
	
	@Autowired
	RequestRepository reqRepo;
	
	@Autowired
	UserRepository userRepositary;

	public List<Request> getAllRequests(){
		return reqRepo.getAllByStatus("In-progress");
	}
	
	@Transactional
	@Override
	public AdminResponseDto approveRequest(AdminRequestDto adminRequestDto) {

		/*
		 * Checking the user is Admin or not
		 * 
		 */
		User user = userRepositary.findByUserId(adminRequestDto.getUserId());
		if (Objects.isNull(user)) {
			throw new UserExcetion("User is Not Available");
		}

		if (!user.isAdmin()) {

			throw new UserExcetion("You dont have a privilizes to Approve/Regect the request");
		}

		Request request = reqRepo.findByUserId(adminRequestDto.getUserId());

		if (Objects.isNull(request)) {

			throw new UserExcetion("Does not have any Request for Connection");
		}

		request.setStatus(adminRequestDto.getStatus());
		request.setComments(adminRequestDto.getComment());

		reqRepo.save(request);
		
		return new AdminResponseDto(adminRequestDto.getRequestId(),adminRequestDto.getStatus());
	}

}
