package com.mobile.application.service;

import com.mobile.application.dto.UserResponse;
import com.mobile.application.entity.Request;
import com.mobile.application.entity.User;

public interface UserService {
	
	public UserResponse userRegistration(User user);

	public Request getRequestDetails(int requestId);
}
