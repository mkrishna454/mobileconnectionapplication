package com.mobile.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobile.application.dto.UserResponse;
import com.mobile.application.entity.Request;
import com.mobile.application.entity.User;
import com.mobile.application.repository.RequestRepository;
import com.mobile.application.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	RequestRepository requestRepo;
	
	@Autowired
	UserRepository userRepo;
	
	public Request getRequestDetails(int requestId) {
		return requestRepo.getAllByRequestId(requestId);
	}
	
	@Override
	public UserResponse userRegistration(User user) {
			userRepo.save(user);
			
			Request request = new Request(user.getUserId(), user.getPlanName(), "INPROGRESS", false, user.getMobileNo());
			saveRequest(request);
				
		return new UserResponse(request.getRequestId(), request.getStatus());
	}
	
	private void saveRequest(Request request) {
		requestRepo.save(request);
	}
}
