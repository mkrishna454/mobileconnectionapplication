package com.mobile.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mobile.application.entity.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {

	public List<Request> getAllByStatus(String Status);
	
	public Request getAllByRequestId(int requestId);
	
	public Request findByUserId(int userId);
	
	public List<Request> findByIsNotifiedFalse();
}
