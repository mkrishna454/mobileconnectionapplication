package com.mobile.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mobile.application.entity.CellNumber;

public interface CellNumberRepository extends JpaRepository<CellNumber, Integer>{
	public CellNumber findByNumberId(long numberId);

}

