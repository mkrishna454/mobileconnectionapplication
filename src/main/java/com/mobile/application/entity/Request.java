package com.mobile.application.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "REQUEST")
public class Request implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int requestId;
	private int userId;
	private String planName;
	private String status;
	private boolean isNotified;
	private String comments;
	private long mobileNumber;
	
	public long getNumberId() {
		return mobileNumber;
	}

	public void setNumberId(long numberId) {
		this.mobileNumber = numberId;
	}
	public Request(int userId, String planId, String status, boolean isNotified, long mobileNo) {
		super();
		this.userId = userId;
		this.planName = planId;
		this.status = status;
		this.isNotified = isNotified;
		this.mobileNumber = mobileNo;
	}

	Request(){
		
	}
	
	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getPlanId() {
		return planName;
	}
	public void setPlanId(String planId) {
		this.planName = planId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isNotified() {
		return isNotified;
	}
	public void setNotified(boolean isNotified) {
		this.isNotified = isNotified;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Object getMobileNumber() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
