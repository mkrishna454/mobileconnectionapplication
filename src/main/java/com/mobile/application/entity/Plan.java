package com.mobile.application.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PLAN_DETAILS")
public class Plan {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int plan_id;
	private String plan_name;
	private String plan_description;
	private int plan_cost;

	public Plan(int plan_id, String plan_name, String plan_description, int plan_cost) {
		super();
		this.plan_id = plan_id;
		this.plan_name = plan_name;
		this.plan_description = plan_description;
		this.plan_cost = plan_cost;
	}
	
	Plan(){
		
	}

	public int getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}

	public String getPlan_name() {
		return plan_name;
	}

	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}

	public String getPlan_description() {
		return plan_description;
	}

	public void setPlan_description(String plan_description) {
		this.plan_description = plan_description;
	}

	public int getPlan_cost() {
		return plan_cost;
	}

	public void setPlan_cost(int plan_cost) {
		this.plan_cost = plan_cost;
	}

}
